<?php
	
	/**
	 * Get data JSON
	 */

	$jsonVector = file_get_contents("./data.json");
	$dataVector = json_decode($jsonVector, true);

	$jsonEndPoint = file_get_contents("./endPoint.json");
	$dataEndPoint = json_decode($jsonEndPoint, true);
	
	/**
	 * Origin = BUNGUR
	 */
	
	 $origin = 1;

	/**
	 * Destination = TOLTANDES
	 */

	$destination = 53;

	/**
	 * Get best direction from BUNGUR to TOLTANDES
	 * Get All Bus
	 * And Filtered By Origin and Destination
	 */

	$dataBus = array();
	foreach($dataVector as $resultKeyBus){

		if(!in_array($resultKeyBus['codeMarge'], @$dataBus)) $dataBus[] = $resultKeyBus['codeMarge'];
	}

	/**
	 * Function get Data By Bus Code
	 */

	function getDataByCode($code, $array){

		$dataVectorByCode = array();
		foreach($array as $resultDataVector){
			if($resultDataVector['codeMarge'] == $code){
				$dataVectorByCode[] = $resultDataVector;
			}
		}

		return $dataVectorByCode;

	}

	/**
	 * Get Bus by filtered
	 */

	$dataBusFiltered = array();
	$indexBusFiltered = 0;
	foreach($dataBus as $resultDataBus){
		
		$isOriginExsists = false;
		$isDestinationExsists = false;

		foreach($dataVector as $resultDataVector){
			if($resultDataVector['codeMarge'] == $resultDataBus){

				/**
				 * Check origin is exsist
				 */
				
				if($resultDataVector['path'][0] == $origin) $isOriginExsists = true;
				if($resultDataVector['path'][1] == $destination) $isDestinationExsists = true;

			}
		}
		
		if($isOriginExsists == true && $isDestinationExsists == true){
			
			$dataBusFiltered[$indexBusFiltered]['code'] = $resultDataBus;
			$dataBusFiltered[$indexBusFiltered]['data'] = getDataByCode($resultDataBus, $dataVector);

			$indexBusFiltered++;

		}

	}

	echo "<pre>";
	print_r($dataBusFiltered);
	echo "</pre>";