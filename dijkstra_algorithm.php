<?php
//set the distance array
$_distArr = array();
$_distArr[1][4] = 2.5;		
$_distArr[4][9] = 5.1;		
$_distArr[9][10] = 1.6;		
$_distArr[10][11] = 1.6;		
$_distArr[11][12] = 1.5;		
$_distArr[12][13] = 0.1;		
$_distArr[13][14] = 0.8;		
$_distArr[14][15] = 1.1;		
$_distArr[15][16] = 1.2;		
$_distArr[16][17] = 1.3;		
$_distArr[17][15] = 1.9;		
$_distArr[15][14] = 1.7;		
$_distArr[14][12] = 0.9;		
$_distArr[12][11] = 1.5;		
$_distArr[11][10] = 3.5;		
$_distArr[10][9] = 0.4;		
$_distArr[9][4] = 4.6;		
$_distArr[4][1] = 4.2;		
$_distArr[9][33] = 0.9;		
$_distArr[33][32] = 2.5;		
$_distArr[32][31] = 1;		
$_distArr[31][30] = 0.7;		
$_distArr[30][29] = 0.7;		
$_distArr[29][28] = 0.6;		
$_distArr[28][27] = 0.5;		
$_distArr[27][26] = 3.7;		
$_distArr[26][25] = 0.2;		
$_distArr[25][24] = 1.1;		
$_distArr[24][23] = 1.1;		
$_distArr[23][25] = 2.6;		
$_distArr[25][27] = 1.5;		
$_distArr[27][32] = 4.1;		
$_distArr[32][9] = 3.3;		
$_distArr[4][5] = 2.7;		
$_distArr[5][6] = 2;		
$_distArr[6][7] = 2.2;		
$_distArr[7][8] = 0.5;		
$_distArr[8][6] = 2.4;		
$_distArr[6][5] = 3.6;		
$_distArr[5][4] = 2.1;		
$_distArr[9][34] = 0.8;		
$_distArr[34][33] = 0.4;		
$_distArr[26][19] = 0.4;		
$_distArr[26][18] = 1;		
$_distArr[18][17] = 1.2;		
$_distArr[19][2] = 3.5;		
$_distArr[2][1] = 12.3;		
$_distArr[34][9] = 2;		
$_distArr[17][27] = 1.1;		
$_distArr[34][35] = 0.9;		
$_distArr[35][39] = 2.7;		
$_distArr[39][40] = 0.4;		
$_distArr[40][41] = 2;		
$_distArr[41][42] = 0.4;		
$_distArr[42][27] = 0.9;		
$_distArr[27][25] = 3.9;		
$_distArr[27][43] = 0.4;		
$_distArr[43][41] = 0.8;		
$_distArr[41][40] = 1.2;		
$_distArr[40][35] = 4.2;		
$_distArr[35][9] = 1.3;		
$_distArr[1][2] = 5.1;		
$_distArr[2][51] = 14.6;		
$_distArr[51][47] = 1;		
$_distArr[47][46] = 0.2;		
$_distArr[46][19] = 1.6;		
$_distArr[19][20] = 0.7;		
$_distArr[20][21] = 2.5;		
$_distArr[21][22] = 3.4;		
$_distArr[22][23] = 1.9;		
$_distArr[17][44] = 0.9;		
$_distArr[44][51] = 3.7;		
$_distArr[51][2] = 0.6;		
$_distArr[9][35] = 0.4;		
$_distArr[35][40] = 4;		
$_distArr[40][49] = 1.4;		
$_distArr[49][48] = 0.5;		
$_distArr[48][47] = 1.3;		
$_distArr[47][50] = 0.5;		
$_distArr[50][53] = 4.1;		
$_distArr[53][52] = 0.4;		
$_distArr[2][53] = 15.7;		
$_distArr[53][2] = 18.1;		
$_distArr[52][2] = 1.7;		
$_distArr[36][37] = 1.1;		
$_distArr[37][38] = 2.9;		
$_distArr[38][53] = 9.1;		
$_distArr[23][19] = 2.5;		
$_distArr[19][47] = 1.2;		
$_distArr[47][51] = 1;		
$_distArr[51][45] = 1.2;		

//the start and the end
$a = 1;
$b = 53;

//initialize the array for storing
$S = array();//the nearest path with its parent and weight
$Q = array();//the left nodes without the nearest path
foreach(array_keys($_distArr) as $val) $Q[$val] = 99999;
$Q[$a] = 0;

//start calculating
while(!empty($Q)){
	$min = array_search(min($Q), $Q);//the most min weight
	if($min == $b) break;
	foreach($_distArr[$min] as $key=>$val) if(!empty($Q[$key]) && $Q[$min] + $val < $Q[$key]) {
		$Q[$key] = $Q[$min] + $val;
		$S[$key] = array($min, $Q[$key]);
	}
	unset($Q[$min]);
}

//list the path
$path = array();
$pos = $b;
while($pos != $a){
	$path[] = $pos;
	$pos = $S[$pos][0];
}
$path[] = $a;
$path = array_reverse($path);

//print result
echo "<img src='RUTE.png' style='width:800px;height:400px;'>";
echo "<br />DARI $a KE $b";
//echo "<br />JARAK TERDEKAT ADALAH ".$S[$b][1]." km";
$hasil = implode(',', $path);
echo "<br />".$hasil;
